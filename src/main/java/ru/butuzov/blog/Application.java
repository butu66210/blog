//  Основной файл, через который запускается наше приложение
// Основной класс, который служит для запуска всего приложения
// Его можно взять из https://spring.io/guides/gs/serving-web-content/#scratch (гайд "Обслуживание веб-контента с помощью Spring MVC")

package ru.butuzov.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
