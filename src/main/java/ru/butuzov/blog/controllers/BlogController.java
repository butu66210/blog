package ru.butuzov.blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.butuzov.blog.models.Post;
import ru.butuzov.blog.repo.PostRepository;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class BlogController {

    //Чтобы передавать все записи в шаблон, нужно создать переменную, которая будет ссылаться на созданный репозиторий

    @Autowired //Нужна, чтобы сослаться на определенный репозиторий
    private PostRepository postRepository;

    @GetMapping("/blog") // Название пути, по которому будем обращаться к шаблону
    public String blogMain(Model model) { // Любой из методов в GetMapping возвращает строку, строка - это просто название того шаблона, который мы должны подключить. Сам метод может называться как угодно

        Iterable<Post> posts = postRepository.findAll();
        //Iterable как массив данных, в котором будут содержаться все значения, полученные от определенной таблички в БД. Здесь указываем ту модель, с которой работаем. Ту модель, которую получаем. Сам объект называем логично posts/
        //метод findAll вытянет все записи из таблички posts
        model.addAttribute("posts", posts);
        // Параметры, передаваемые в шаблон. Эта строчка кода нужно только тогда, когда нужен не простой вывод страницы, а ввод каких-то данных на нее
        //Сначала указываем название атрибута, потом что в него передаем
        return "blog-main"; // Название возвращаемого шаблона
        //Все записи находим в определенной табличке и передаем в шаблон blog-main. Теперь нужно прописать код, чтобы оботражать все эти записи внутри шаблона
    }

    @GetMapping("/blog/add")
    public String blogAdd(Model model) {
        return "blog-add";
    }

    @PostMapping("/blog/add")
    public String blogPostAdd(@RequestParam String title, @RequestParam String anons, @RequestParam String full_text, Model model) { //RequestParam - получение новых параметров, далее тип получаемых данных и название из поля для ввода
        Post post = new Post(title, anons, full_text); //Создали объект внутри программы
        postRepository.save(post); //Добавляем объект в БД (новая статья)
        return "redirect:/blog"; //Перенаправляем на страницу блог
    }

    @GetMapping("/blog/{id}") //Динамический параметр в фигурных скобках. Название может быть каким угодно
    public String blogDetails(@PathVariable(value = "id") long id, Model model) {
        if (!postRepository.existsById(id)) { //Метод возвращает tru, если запись найдена или false, если не найдена
            return "redirect:/blog";
        }
        //Чтобы брать динамическое значение из URL-адреса, нужно прописать аннотацию @PathVariable и указываем какой динамический параметр принимаем в круглых скобках
        //Далее тип параметра и какое угодно название
        Optional<Post> post = postRepository.findById(id); //Запись помещаем в объект на основе класса Optional, где в <> указываем с какой моделью будем работать
        //Из БД найти определенную нужную запись и отобразить ее в шаблоне
        ArrayList<Post> res = new ArrayList<>(); //Чтобы внедрить в шаблон Optional лучше его перевести в ArrayList, чтобы не было ошибок. Здесь пока просто выделяем память
        post.ifPresent(res::add); //Здесь с помощью метода ifPresent происходит переход
        model.addAttribute("post", res);
        return "blog-details";
    }

    @GetMapping("/blog/{id}/edit")
    public String blogEdit(@PathVariable(value = "id") long id, Model model) {
        if (!postRepository.existsById(id)) {
            return "redirect:/blog";
        }
        Optional<Post> post = postRepository.findById(id);
        ArrayList<Post> res = new ArrayList<>();
        post.ifPresent(res::add);
        model.addAttribute("post", res);
        return "blog-edit";
    }

    @PostMapping("/blog/{id}/edit")
    public String blogPostUpdate(@PathVariable(value = "id") long id, @RequestParam String title, @RequestParam String anons, @RequestParam String full_text, Model model) { //RequestParam - получение новых параметров, далее тип получаемых данных и название из поля для ввода
        Post post = postRepository.findById(id).orElseThrow(); //Чтобы ошибка не выскакивала, нужно прописывать, .orElseThrow() после вызова функции. Этот метод выбросит исключение, если по id ничего не будет найдено
        post.setTitle(title); //Устанавливаем название
        post.setAnons(anons); //Устанавливаем анонс
        post.setFull_text(full_text); //Устанавливаем текст
        postRepository.save(post); //Сохраняем в БД. Новый объект не будет создан. Будет обновлен старый из-за того, что мы пост не делали через new, а сделали через поиск в репозитории
        return "redirect:/blog"; //Перенаправляем на страницу блога
    }

    @PostMapping ("/blog/{id}/remove") //Синтаксис отличается от пропитания в шаблоне
    public String blogPostDelete(@PathVariable(value = "id") long id, Model model) { //RequestParam - получение новых параметров, далее тип получаемых данных и название из поля для ввода
        Post post = postRepository.findById(id).orElseThrow(); //Чтобы ошибка не выскакивала, нужно прописывать, .orElseThrow() после вызова функции. Этот метод выбросит исключение, если по id ничего не будет найден
        postRepository.delete(post); //Удаляем пост из БД
        return "redirect:/blog"; //Перенаправляем на страницу блога
    }


}
