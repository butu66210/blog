//  Код для создания контроллера берется с https://spring.io/guides/gs/serving-web-content/#scratch (гайд "Обслуживание веб-контента с помощью Spring MVC")
//  Контроллеры пишутся в папке src/main/java/ru/butuzov/blog/controllers/
//  Или без определенной папки, если контроллер один - src/main/java/ru/butuzov/blog/
//  Но хорошей практикой считается собирать все контроллеры в отдельную папку, даже если он один

package ru.butuzov.blog.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller // Класс, который отвечает за обработку всех переходов на сайте
public class MainController {

    // Прописываем обработку главной странички. Что будет происходить при переходе на нее
    @GetMapping("/") // Метод обрабатывает определенный URL-адрес. В данном случае главная страница
    public String home(Model model) { // home - название метода. model - обязатальный параметр, при помощи которого мы указываем какие данные мы можем передать внутрь шаблона (HTML-файла)
        model.addAttribute("title", "Главная страница"); //В параметр (свойство) по названию title внутри шаблона (HTML-файла) передаем текст
        return "home"; // Вызываем определенный шаблон (HTML-файл) по его названию
    }

    // Поменять часть кода внутри
    @GetMapping("/about")
    public String about(Model model) {
        model.addAttribute("title", "Страница про нас");
        return "about";
    }


}
