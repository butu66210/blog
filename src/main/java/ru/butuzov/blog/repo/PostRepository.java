package ru.butuzov.blog.repo;

import org.springframework.data.repository.CrudRepository;//Уже готовый интерфейс, в котором есть готовые методы, чтобы добавить данные в табличку, удалить данные из таблички, обновить и удалить (CRUD)
import ru.butuzov.blog.models.Post;

public interface PostRepository extends CrudRepository <Post, Long>{ // Указываем модель, с которой работаем и тип дыннх уникального идентификатора. Больше ничего прописывать не нужно. Все методы унаследовались. С ними можно работать
}
