package ru.butuzov.blog.models;
//MVC - шаблоны, контроллеры, модели
//Здесь мы просто создаем табличку, а чтобы ею манипулировать, нужно сделать интерфейс репозиторий
//Для каждой модели нужно создавать свой репозиторий

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity //Модель (сущность)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Post {

    @Id //Уникальный идентификатор
    @GeneratedValue(strategy = GenerationType.AUTO) //Всегда новый уникальный идентификатор будет автоматически
    private Long id;

    private String title, anons, full_text;

    private int views;

    public Post(String title, String anons, String full_text) {
        this.title = title;
        this.anons = anons;
        this.full_text = full_text;
    }
}
